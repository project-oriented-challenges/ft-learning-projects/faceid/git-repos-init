#!/usr/bin/env python

import csv

# It is assumed that the script is run in an environment that has the eth_account
# module installed.
# It can be done for example in the anaconda docker:
# $ sudo docker pull continuumio/anaconda3
# $ sudo docker run -i -t --rm -p 8888:8888 continuumio/anaconda3 \
#     /bin/bash -c "/opt/conda/bin/conda install jupyter -y --quiet; \
#     pip install eth_account; \
#     mkdir /opt/notebooks; \
#     /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='0.0.0.0' --port=8888 --no-browser --allow-root"


# The script expects the csv file that keeps informaton about repos in form of
# <group-of-participants>;<group-of-projects>

new_file_data = {}

META_GROUP="onti-2019-fintech/"
#META_GROUP=""

exec_file = "#!/bin/bash\n\n"

with open('repos.csv') as csvfile:
    d = csv.reader(csvfile, delimiter=";")
    for row in d:
        print(row)
        i = row[1]
        gr = row[0]
        exec_file+=f'echo "generate repo for {i}"\n'
        exec_file+="cp -a template repo\n"
        exec_file+="cd repo\n"
        exec_file+="git init\n"
        #exec_file+=f'git remote add origin git@gitlab.com:alex.kolotov/test-and-remove.git\n'
        if len(META_GROUP) != 0:
            exec_file+=f'git remote add origin git@gitlab.com:{META_GROUP}{gr}/{i}/faceid.git\n'
        else:
            exec_file+=f'git remote add origin git@gitlab.com:{gr}/{i}/faceid.git\n'
        exec_file+="git add .\n"
        exec_file+="git commit -m 'Initial commit from template [skip ci]'\n"
        exec_file+="git push -u origin master\n"
        exec_file+="cd ..\n"
        exec_file+="rm -rf repo\n\n"
    
with open('init-repos.sh', 'w') as script_file:
    script_file.write(exec_file)
